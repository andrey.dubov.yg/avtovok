﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IDBRepos
    {
        IRepository<Bus> Bus { get; }
        IRepository<Driver> Driver { get; }
        IRepository<Ticket> Ticket { get; }
        IRepository<Layout> Layout { get; }
        IRepository<Stop> Stop { get; }
        IRepository<Race> Race { get; }
        IRepository<Route> Route { get; }

        int Save();
    }
}
