namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBContext : DbContext
    {
        public DBContext()
            : base("name=DBContext")
        {
        }

        public virtual DbSet<Bus> Buses { get; set; }
        public virtual DbSet<Driver> Drivers { get; set; }
        public virtual DbSet<Layout> Layouts { get; set; }
        public virtual DbSet<Race> Races { get; set; }
        public virtual DbSet<Route> Routes { get; set; }
        public virtual DbSet<Stop> Stops { get; set; }
        public virtual DbSet<Ticket> Tickets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bus>()
                .Property(e => e.Number)
                .IsUnicode(false);

            modelBuilder.Entity<Bus>()
                .HasMany(e => e.Races)
                .WithRequired(e => e.Bus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Driver>()
                .Property(e => e.Fio)
                .IsUnicode(false);

            modelBuilder.Entity<Driver>()
                .HasMany(e => e.Races)
                .WithRequired(e => e.Driver)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Layout>()
                .Property(e => e.Number_of_sits)
                .IsFixedLength();

            modelBuilder.Entity<Layout>()
                .Property(e => e.Mark)
                .IsUnicode(false);

            modelBuilder.Entity<Layout>()
                .HasMany(e => e.Buses)
                .WithRequired(e => e.Layout)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Race>()
                .HasMany(e => e.Tickets)
                .WithRequired(e => e.Race)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Route>()
                .Property(e => e.Name_of_route)
                .IsUnicode(false);

            modelBuilder.Entity<Route>()
                .Property(e => e.Travel_time)
                .IsUnicode(false);

            modelBuilder.Entity<Route>()
                .HasMany(e => e.Races)
                .WithRequired(e => e.Route)
                .HasForeignKey(e => e.Rout_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Route>()
                .HasMany(e => e.Stops)
                .WithRequired(e => e.Route)
                .HasForeignKey(e => e.Rout_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Stop>()
                .Property(e => e.Name)
                .IsUnicode(false);
        }
    }
}
