﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class RaceRepositorySQL : IRepository<Race>
    {
        private DBContext db;

        public RaceRepositorySQL(DBContext dbcontext)
        {
            this.db = dbcontext;
            db.Races.Include("Bus").Load();
        }

        public ObservableCollection<Race> GetList()
        {
            return db.Races.Local;
        }

        public Race GetItem(int id)
        {
            return db.Races.Find(id);
        }

        public void Create(Race c)
        {
            db.Races.Add(c);
        }

        public void Update(Race c)
        {
            db.Entry(c).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Race c = db.Races.Find(id);
            if (c != null)
                db.Races.Remove(c);
        }

    }
}
