﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class LayoutRepositorySQL : IRepository<Layout>
    {
        private DBContext db;

        public LayoutRepositorySQL(DBContext dbcontext)
        {
            this.db = dbcontext;
            db.Layouts.Load();
        }

        public ObservableCollection<Layout> GetList()
        {
            return db.Layouts.Local;
        }

        public Layout GetItem(int id)
        {
            return db.Layouts.Find(id);
        }

        public void Create(Layout c)
        {
            db.Layouts.Add(c);
        }

        public void Update(Layout c)
        {
            db.Entry(c).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Layout c = db.Layouts.Find(id);
            if (c != null)
                db.Layouts.Remove(c);
        }

    }
}
