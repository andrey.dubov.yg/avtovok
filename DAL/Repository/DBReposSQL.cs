﻿using DAL.Interfaces;

namespace DAL.Repository
{
    public class DBReposSQL : IDBRepos
    {
        private DBContext db;
        private BusRepositorySQL BRepository;
        private StopRepositorySQL SRepository;
        private DriverRepositorySQL DRepository;
        private LayoutRepositorySQL LRepository;
        private TicketRepositorySQL TRepository;
        private RouteRepositorySQL RoRepository;
        private RaceRepositorySQL RaRepository;

        public DBReposSQL()
        {
            db = new DBContext();
        }

        public IRepository<Bus> Bus
        {
            get
            {
                if (BRepository == null)
                    BRepository = new BusRepositorySQL(db);
                return BRepository;
            }
        }

        public IRepository<Race> Race
        {
            get
            {
                if (RaRepository == null)
                    RaRepository = new RaceRepositorySQL(db);
                return RaRepository;
            }
        }

        public IRepository<Route> Route
        {
            get
            {
                if (RoRepository == null)
                    RoRepository = new RouteRepositorySQL(db);
                return RoRepository;
            }
        }

        public IRepository<Ticket> Ticket
        {
            get
            {
                if (TRepository == null)
                    TRepository = new TicketRepositorySQL(db);
                return TRepository;
            }
        }

        public IRepository<Driver> Driver
        {
            get
            {
                if (DRepository == null)
                    DRepository = new DriverRepositorySQL(db);
                return DRepository;
            }
        }

        public IRepository<Layout> Layout
        {
            get
            {
                if (LRepository == null)
                    LRepository = new LayoutRepositorySQL(db);
                return LRepository;
            }
        }

        public IRepository<Stop> Stop
        {
            get
            {
                if (SRepository == null)
                    SRepository = new StopRepositorySQL(db);
                return SRepository;
            }
        }

        //public IObjRepos Object
        //{
        //    get
        //    {
        //        if (ORepository == null)
        //            ORepository = new ObjectRepositorySQL(db);
        //        return ORepository;
        //    }
        //}
        //public ISdelRepos Sdel
        //{
        //    get
        //    {
        //        if (SRepository == null)
        //            SRepository = new SdelkaRepositorySQL(db);
        //        return SRepository;
        //    }
        //}



        public int Save()
        {
            return db.SaveChanges();
        }
   
    }
}
