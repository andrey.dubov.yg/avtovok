﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class TicketRepositorySQL : IRepository<Ticket>
    {
        private DBContext db;

        public TicketRepositorySQL(DBContext dbcontext)
        {
            this.db = dbcontext;
            db.Buses.Load();
        }

        public ObservableCollection<Ticket> GetList()
        {
            return db.Tickets.Local;
        }

        public Ticket GetItem(int id)
        {
            return db.Tickets.Find(id);
        }

        public void Create(Ticket c)
        {
            db.Tickets.Add(c);
        }

        public void Update(Ticket c)
        {
            db.Entry(c).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Ticket c = db.Tickets.Find(id);
            if (c != null)
                db.Tickets.Remove(c);
        }

    }
}
