﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class BusRepositorySQL : IRepository<Bus>
    {
        private DBContext db;

        public BusRepositorySQL(DBContext dbcontext)
        {
            this.db = dbcontext;
            db.Buses.Include("Layout").Load();
        }

        public ObservableCollection<Bus> GetList()
        {
            return db.Buses.Local;
        }

        public Bus GetItem(int id)
        {
            return db.Buses.Find(id);
        }

        public void Create(Bus c)
        {
            db.Buses.Add(c);
        }

        public void Update(Bus c)
        {
            db.Entry(c).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Bus c = db.Buses.Find(id);
            if (c != null)
                db.Buses.Remove(c);
        }

    }
}
