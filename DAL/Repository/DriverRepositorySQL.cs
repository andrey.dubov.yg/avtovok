﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class DriverRepositorySQL : IRepository<Driver>
    {
        private DBContext db;

        public DriverRepositorySQL(DBContext dbcontext)
        {
            this.db = dbcontext;
            db.Drivers.Include("Layout").Load();
        }

        public ObservableCollection<Driver> GetList()
        {
            return db.Drivers.Local;
        }

        public Driver GetItem(int id)
        {
            return db.Drivers.Find(id);
        }

        public void Create(Driver c)
        {
            db.Drivers.Add(c);
        }

        public void Update(Driver c)
        {
            db.Entry(c).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Driver c = db.Drivers.Find(id);
            if (c != null)
                db.Drivers.Remove(c);
        }

    }
}
