﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class RouteRepositorySQL : IRepository<Route>
    {
        private DBContext db;

        public RouteRepositorySQL(DBContext dbcontext)
        {
            this.db = dbcontext;
            db.Routes.Load();
        }

        public ObservableCollection<Route> GetList()
        {
            return db.Routes.Local;
        }

        public Route GetItem(int id)
        {
            return db.Routes.Find(id);
        }

        public void Create(Route c)
        {
            db.Routes.Add(c);
        }

        public void Update(Route c)
        {
            db.Entry(c).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Route c = db.Routes.Find(id);
            if (c != null)
                db.Routes.Remove(c);
        }

    }
}
