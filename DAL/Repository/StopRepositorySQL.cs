﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class StopRepositorySQL : IRepository<Stop>
    {
        private DBContext db;

        public StopRepositorySQL(DBContext dbcontext)
        {
            this.db = dbcontext;
            db.Buses.Load();
        }

        public ObservableCollection<Stop> GetList()
        {
            return db.Stops.Local;
        }

        public Stop GetItem(int id)
        {
            return db.Stops.Find(id);
        }

        public void Create(Stop c)
        {
            db.Stops.Add(c);
        }

        public void Update(Stop c)
        {
            db.Entry(c).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Stop c = db.Stops.Find(id);
            if (c != null)
                db.Stops.Remove(c);
        }

    }
}
