namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Stop")]
    public partial class Stop
    {
        [Key]
        public int Stop_id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int Stop_number { get; set; }

        public int Rout_id { get; set; }

        public int Price { get; set; }

        public virtual Route Route { get; set; }
    }
}
