namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ticket")]
    public partial class Ticket
    {
        [Key]
        public int Ticket_id { get; set; }

        public int Sit_number { get; set; }

        public int Race_id { get; set; }

        public virtual Race Race { get; set; }
    }
}
