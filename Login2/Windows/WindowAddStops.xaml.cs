﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Login2.Windows
{
    /// <summary>
    /// Логика взаимодействия для WindowAddStops.xaml
    /// </summary>
    public partial class WindowAddStops : Window
    {
        Route ro;
        DBContext db = new DBContext();
        public WindowAddStops(int r)
        {
            db.Routes.Load();
            db.Stops.Load();
            ro = db.Routes.Find(r);
            
            //db.Routes.Add(r);
            InitializeComponent();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (d1.Text != "" && d2.Text != "")
            {
                //ro = db.Routes.Find(ro.Route_id);
                Stop s = new Stop();
                
                var sList = db.Stops.Where(p => p.Rout_id == ro.Route_id);
                s.Name = d1.Text;                
                s.Stop_number = sList.ToList().Count+1;
                s.Price = int.Parse(d2.Text);
                s.Rout_id=ro.Route_id;
                db.Stops.Add(s);
                if (sList.ToList().Count == 0)
                {
                        ro.Name_of_route = s.Name;
                }
                else
                {
                    Stop s1 = db.Stops.Where(p => p.Rout_id == ro.Route_id).FirstOrDefault();
                    ro.Name_of_route = s1.Name + "-" + s.Name;
                }
                
                db.SaveChanges();
                this.Close();
            }
        }
    }
}
