﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Threading;

namespace Login2
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class AdmWindow : Window
    {

        public string AdminPassword = "12345";
        public AdmWindow()
        {
            InitializeComponent();
        }

        private async void  Button_Click(object sender, RoutedEventArgs e)
        {
            if(PasswordA.Text != AdminPassword)
            {
                CallPopup("НЕВЕРНЫЙ ИДЕНТИФИКАТОР");

            }
            else
            {
                //bar.IsIndeterminate = true;
                ////CallPopup("ЗАГРУЗКА...");

                //bars();
                Thread.Sleep(8000);
                //await Task.Run(() => bars());
                Show();
            }
        }

        //void bars()
        //{

        //    //bar.IsIndeterminate = true;
        //    bar.Visibility = Visibility.Visible;
            
        //    CallPopup("ЗАГРУЗКА...");

        //}

        void Show()
        {
            AdminWork a = new AdminWork(true);
            a.Show();
            Close();
        }

        public void CallPopup(string text)
        {
            popupMessage.Text = text;
            Storyboard s = (Storyboard)this.TryFindResource("ShowPopup");
            BeginStoryboard(s);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Start s = new Start();
            s.Show();
            Close();
        }

     
    }
}
