﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Login2.Pages.SettingsPages;

namespace Login2.Windows
{
    /// <summary>
    /// Логика взаимодействия для SettingWindow.xaml
    /// </summary>
    public partial class SettingWindow : Window
    {
        DriverPage dpage;
        BusPage bpage;
        RoutPage rpage;
        int selectedTab;
        public SettingWindow()
        {
            InitializeComponent();
            double screenHeight = SystemParameters.FullPrimaryScreenHeight;
            double screenWidth = SystemParameters.FullPrimaryScreenWidth;

            dpage = new DriverPage();
            bpage = new BusPage();
            rpage = new RoutPage();

            this.Top = (screenHeight - this.Height) / 0x00000002;
            this.Left = (screenWidth - this.Width) / 0x00000002;
            selectedTab = 0;

            selectedTab = 0;

            buttonDriver.Background = Brushes.White;
            buttonDriver.Foreground = Brushes.Black;

            dpage.RefreshListBoxUser();
            Main.Content = dpage;

            gridCursor.Background = new SolidColorBrush(Color.FromRgb(156, 39, 176));
            gridCursor.Width = buttonDriver.ActualWidth;
        }

        

        private void driverClick(object sender, RoutedEventArgs e)
        {
            dpage.RefreshListBoxUser();
            Main.Content = dpage;

            selectedTab = 0;
            gridCursor.Margin = new Thickness((gridCursor.Width * selectedTab), 0, 0, 0);

            buttonDriver.Background = Brushes.White;
            buttonDriver.Foreground = Brushes.Black;

            buttonBus.Background = new SolidColorBrush(Color.FromRgb(33, 150, 243));
            buttonBus.Foreground = Brushes.White;

            buttonRout.Background = new SolidColorBrush(Color.FromRgb(33, 150, 243));
            buttonRout.Foreground = Brushes.White;

           
        }

        private void busClick(object sender, RoutedEventArgs e)
        {
            //bpage.RefreshListBoxType();
            Main.Content = bpage;

            selectedTab = 1;
            gridCursor.Margin = new Thickness((gridCursor.Width * selectedTab), 0, 0, 0);

            buttonBus.Background = Brushes.White;
            buttonBus.Foreground = Brushes.Black;

            buttonDriver.Background = new SolidColorBrush(Color.FromRgb(33, 150, 243));
            buttonDriver.Foreground = Brushes.White;

            buttonRout.Background = new SolidColorBrush(Color.FromRgb(33, 150, 243));
            buttonRout.Foreground = Brushes.White;
        }

        private void routClick(object sender, RoutedEventArgs e)
        {
             Main.Content = rpage;

            selectedTab = 2;
            gridCursor.Margin = new Thickness((gridCursor.Width * selectedTab), 0, 0, 0);

            buttonRout.Background = Brushes.White;
            buttonRout.Foreground = Brushes.Black;

            buttonDriver.Background = new SolidColorBrush(Color.FromRgb(33, 150, 243));
            buttonDriver.Foreground = Brushes.White;

            buttonBus.Background = new SolidColorBrush(Color.FromRgb(33, 150, 243));
            buttonBus.Foreground = Brushes.White;
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
