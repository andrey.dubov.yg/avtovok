﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DAL;

namespace Login2.Windows
{
    /// <summary>
    /// Логика взаимодействия для WindowTicket.xaml
    /// </summary>
    public partial class WindowTicket : Window
    {
        public DBContext db = new DBContext();
        int index, s;
        public WindowTicket(int id, int Stop_id)
        {
            index = id;
            s = Stop_id;
            InitializeComponent();
            

            db.Drivers.Load();
            db.Buses.Load();
            db.Routes.Load();
            db.Tickets.Load();
            db.Layouts.Load();
            FillDataGrid();

        }

        private void Print(object sender, RoutedEventArgs e)
        {

            PrintDialog printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == true)
            {              
                printDialog.PrintDocument(((IDocumentPaginatorSource)doc.Document).DocumentPaginator, "Распечатываем элемент Canvas");
            }
            this.Close();
        }
        public void FillDataGrid()
        {

            //var resultGrid = from race in db.Races.Local.ToList()
            //                 join t in db.Tickets.Local on race.Race_id equals t.Race_id

            //                  where race.Race_id == index

            //                 select new Ticket
            //                 {

            //                     Race_id = race.Race_id,
            //                     Ticket_id = t.Ticket_id,
            //                     Sit_number = t.Sit_number

            //                 };
            var ticketsList = db.Tickets.Where(p => p.Race_id == index);


            Race r = db.Races.Find(index);
            Driver d = db.Drivers.Find(r.Driver_id);
            Bus b = db.Buses.Find(r.Bus_id);
            Layout l = db.Layouts.Find(b.Layout_id);
            Route ro = db.Routes.Find(r.Rout_id);
            Stop st = db.Stops.Find(s);
            int ssd = ticketsList.ToList().Count;
            if (int.Parse(l.Number_of_sits) != ticketsList.ToList().Count)
            {
                Ticket t = new Ticket();
                t.Race_id = index;
                
                Date.Text = r.Data.ToShortDateString();
                Route.Text = ro.Name_of_route;
                BuseN.Text = b.Number;
                BuseM.Text = l.Mark;
                Driver.Text = d.Fio;
                
                t.Sit_number = ticketsList.ToList().Count + 1;
                Price.Text = st.Price.ToString();
                db.Tickets.Add(t);
                db.SaveChanges();
                Number.Text = t.Ticket_id.ToString();             
                Place.Text = t.Sit_number.ToString();

            }  
           
        }
    }
}
