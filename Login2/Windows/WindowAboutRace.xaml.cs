﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DAL;
using Path = System.IO.Path;

namespace Login2.Windows
{
    /// <summary>
    /// Логика взаимодействия для WindowAboutRace.xaml
    /// </summary>
    public partial class WindowAboutRace : Window
    {
        public DBContext db;
        int index;
        public WindowAboutRace(int ind)
        {
            index = ind;
            db = new DBContext();
            InitializeComponent();
            //var path = Path.Combine(Environment.CurrentDirectory, "Layouts", "1.jpg");
            //var uri = new Uri(path);
            //var bitmap = new BitmapImage(uri);
            //i.Source = bitmap;
            FillDataGrid();


        }
        public void FillDataGrid()
        {
           

            Race r = db.Races.Find(index);
            Route ro = db.Routes.Find(r.Rout_id);

            Bus b = db.Buses.Find(r.Bus_id);
            Layout l = db.Layouts.Find(b.Layout_id);
            Driver dr = db.Drivers.Find(r.Driver_id);
            var ticketsList = db.Tickets.Where(p => p.Race_id == r.Race_id);

            numberofPlaces.Text = l.Number_of_sits;
            BusNumber.Text = b.Number;
            Fio.Text = dr.Fio;
            Kol_stops.Text = ro.Travel_time;
            Kol_place.Text = (int.Parse(l.Number_of_sits) - ticketsList.ToList().Count).ToString();
            Route.Text = ro.Name_of_route;
        }

    }
}
