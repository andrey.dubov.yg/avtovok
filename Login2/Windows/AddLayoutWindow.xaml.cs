﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DAL;

namespace Login2.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddLayoutWindow.xaml
    /// </summary>
    public partial class AddLayoutWindow : Window
    {
        DBContext db;
        List<Layout> list;
        public AddLayoutWindow()
        {
            db = new DBContext();
            InitializeComponent();
            Refresh();
            double screenHeight = SystemParameters.FullPrimaryScreenHeight;
            double screenWidth = SystemParameters.FullPrimaryScreenWidth;

            this.Top = (screenHeight - this.Height) / 0x00000002;
            this.Left = (screenWidth - this.Width) / 0x00000002;
            //MaterialDesignThemes.Wpf.Flipper flipper = Card1;
            //flipper.Name = "Card2";
            //flipper.
            //CardPanel.Children.Add(flipper);
        }

        void Refresh()
        {
            db.Layouts.Load();
            list = db.Layouts.Local.ToList();
            Layouts.ItemsSource = list;
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Layout l = new Layout();
            if (markTextBox.Text != "" || numberTextBox.Text != "")
            {
                l.Mark = markTextBox.Text;
                l.Number_of_sits = numberTextBox.Text;
                db.Layouts.Add(l);
                db.SaveChanges();
            }

            markTextBox.Text = "";
            numberTextBox.Text = "";

            Refresh();



        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
