﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Login2.Windows
{
    /// <summary>
    /// Логика взаимодействия для DriverEd.xaml
    /// </summary>
    public partial class DriverEd : Window
    {
        DBContext db = new DBContext();
        public Driver driver;
        public DriverEd(Driver d)
        {
            driver = d;
            InitializeComponent();
            d1.Text = d.Fio;
            d2.Text = d.Age.ToString();
            d3.Text = d.Number_of_races.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (d1.Text != "" && d2.Text != "" && d3.Text != "")
            {
                driver = db.Drivers.Find(driver.Driver_id);
                driver.Fio = d1.Text;
                driver.Age = int.Parse(d2.Text);
                driver.Number_of_races = int.Parse(d3.Text);
                //db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();
                this.Close();
            }
        }
    }
}
