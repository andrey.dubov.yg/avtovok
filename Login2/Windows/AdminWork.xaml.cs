﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Login2
{
    /// <summary>
    /// Логика взаимодействия для AdminWork.xaml
    /// </summary>
    public partial class AdminWork : Window
    {
        bool maxSize = false;
        Pages.SearchFilter pageFilter;
        Pages.AddRacePage pageAdd;
        bool rezim;
        public AdminWork(bool rez)
        {
            rezim = rez;
            InitializeComponent();
            pageFilter = new Pages.SearchFilter(rez);
            pageAdd = new Pages.AddRacePage();
            Main.Content = pageFilter;
            if(!rez)
            {
                Set.Visibility = Visibility.Collapsed;
                L.Visibility = Visibility.Collapsed;
                R.Visibility = Visibility.Collapsed;

            } else
            {
                Set.Visibility = Visibility.Visible;
                L.Visibility = Visibility.Visible;
                R.Visibility = Visibility.Visible;

            }
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!maxSize)
            {
                this.WindowState = WindowState.Maximized;
                maxSize = true;
            }
            else
            {
                this.WindowState = WindowState.Normal;
                maxSize = false;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (Main.Content != pageAdd)
            {
                pageAdd = new Pages.AddRacePage();
                Main.Content = pageAdd;
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (Main.Content != pageFilter)
            {
                pageFilter = new Pages.SearchFilter(true);
                pageFilter.FillDataGrid();
                Main.Content = pageFilter;
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            Windows.SettingWindow newWindow = new Windows.SettingWindow();
            newWindow.ShowDialog();
            if (Main.Content == pageFilter)
            {
                pageFilter = new Pages.SearchFilter(true);
                pageAdd = new Pages.AddRacePage();
                Main.Content = pageFilter;

            }
            else
            {
                pageFilter = new Pages.SearchFilter(true);
                pageAdd = new Pages.AddRacePage();
                Main.Content = pageAdd;

            }

        }
    }
}
