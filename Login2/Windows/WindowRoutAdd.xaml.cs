﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DAL;
namespace Login2.Windows
{
    /// <summary>
    /// Логика взаимодействия для WindowRoutAdd.xaml
    /// </summary>
    public partial class WindowRoutAdd : Window
    {
        DBContext db = new DBContext();
        Route r = new Route();
        public WindowRoutAdd()
        {
            InitializeComponent();
            r.Travel_time = "a";
            r.Name_of_route = "a";
            db.Routes.Add(r);
            db.SaveChanges();
            // r = db.Routes.Find(id);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (rTime.Text != "")
            {
                db.Stops.Load();                
                r.Travel_time = rTime.Text;
                db.SaveChanges();
                WindowAddStops s = new WindowAddStops(r.Route_id);
                s.ShowDialog();
                
                Stop.ItemsSource = db.Stops.Where(p => p.Rout_id == r.Route_id).ToList();

            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            
                //r.Travel_time = rTime.Text;
                //r.Name_of_route = "ф";
                //db.Routes.Add(r);
                db.SaveChanges();
                this.Close();
            
        }
    }
}
