﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Login2.Windows
{
    /// <summary>
    /// Логика взаимодействия для CardFlip1.xaml
    /// </summary>
    public partial class CardFlip1 : UserControl
    {
        DBContext db;
        Layout l = new Layout();
        public CardFlip1()
        {
            db = new DBContext();
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            l.Layout_id = int.Parse(id.Text);
            l = db.Layouts.Find(l.Layout_id);
            if (Mark.Text != "" && Sits.Text != "")
            {
                l.Mark = Mark.Text;
                l.Number_of_sits = Sits.Text;
                db.Entry(l).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
