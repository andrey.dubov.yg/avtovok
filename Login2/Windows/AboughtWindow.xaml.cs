﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Data.Entity;
using DAL;

namespace Login2.Windows
{
    /// <summary>
    /// Логика взаимодействия для AboughtWindow.xaml
    /// </summary>
    public partial class AboughtWindow : Window
    {

        public Race race { get; private set; }
        bool initialize = false;
        public DBContext db;

        public AboughtWindow(Race r)
        {
            InitializeComponent();
            race = r;
            db = new DBContext();
            db.Drivers.Load();
            db.Buses.Load();
            db.Routes.Load();

            double screenHeight = SystemParameters.FullPrimaryScreenHeight;
            double screenWidth = SystemParameters.FullPrimaryScreenWidth;

            this.Top = (screenHeight - this.Height) / 0x00000002;
            this.Left = (screenWidth - this.Width) / 0x00000002;

            fill_addPage();

        }


        void fill_addPage()
        {
        
            comboboxDriver.ItemsSource = db.Drivers.Local;      
            comboboxBus.ItemsSource = db.Buses.Local;
            comboboxRout.ItemsSource = db.Routes.Local;
            RaceData.SelectedDate = race.Data;


            
            
            comboboxRout.SelectedItem = db.Routes.Find(race.Rout_id);
            comboboxBus.SelectedItem = db.Buses.Find(race.Bus_id);
            comboboxDriver.SelectedItem = db.Drivers.Find(race.Driver_id);


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Bus b = (Bus)comboboxBus.SelectedItem;
            race.Bus_id = b.Bus_id;
            Driver d = (Driver)comboboxDriver.SelectedItem;
            race.Driver_id = d.Driver_id;
            Route r = (Route)comboboxRout.SelectedItem;
            race.Rout_id = r.Route_id;
            db.Entry(race).State = EntityState.Modified;
        }
    }
}
