﻿#pragma checksum "..\..\..\Windows\AboughtWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2ADBAEE7F5AAB2B47249EAD25119A5F2C07EF1C8"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using Login2.Windows;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Login2.Windows {
    
    
    /// <summary>
    /// AboughtWindow
    /// </summary>
    public partial class AboughtWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 33 "..\..\..\Windows\AboughtWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid popupMessageGrid;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Windows\AboughtWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock popupMessage;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\Windows\AboughtWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textboxAddPrice;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\Windows\AboughtWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textboxAddTime;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\Windows\AboughtWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboboxDriver;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\..\Windows\AboughtWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboboxBus;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\Windows\AboughtWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker RaceData;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\..\Windows\AboughtWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboboxRout;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Login2;component/windows/aboughtwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Windows\AboughtWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.popupMessageGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.popupMessage = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.textboxAddPrice = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.textboxAddTime = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.comboboxDriver = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.comboboxBus = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.RaceData = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 8:
            this.comboboxRout = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            
            #line 111 "..\..\..\Windows\AboughtWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

