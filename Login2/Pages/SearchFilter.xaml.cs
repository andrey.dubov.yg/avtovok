﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using DAL;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Login2.Windows;

namespace Login2.Pages
{
    /// <summary>
    /// Логика взаимодействия для SearchFilter.xaml
    /// </summary>
    public partial class SearchFilter : Page
    {
        public DBContext db;
        public SearchFilter(bool adm)
        {
            InitializeComponent();
            if (adm == false)
                DeleteRace.Visibility = Visibility.Collapsed;
            db = new DBContext();
            db.Races.Load();
            //RaceGrid.ItemsSource = db.Races.Local.ToList();
            FillDataGrid();
        }

        public void FillDataGrid()
        {
            var resultGrid = from r in db.Races.Local.ToList()
                             join b in db.Buses on r.Bus_id equals b.Bus_id
                             join d in db.Drivers on r.Driver_id equals d.Driver_id
                             join ro in db.Routes on r.Rout_id equals ro.Route_id
                             join st in db.Stops on ro.Route_id equals st.Rout_id
                             join l in db.Layouts on b.Layout_id equals l.Layout_id
                             // where st.Stop_number.Max()
                             where r.Data >= DateTime.Today
                             select new
                             {
                                 Data = r.Data.ToShortDateString(),
                                 Driver = d.Fio,
                                 Bus = b.Number,
                                 Travel_time = ro.Travel_time,
                                 Price = st.Price.ToString() + " руб.",
                                 Sets = l.Number_of_sits,
                                 Route = st.Name,

                                 Bus_id = r.Bus_id,
                                 Race_id = r.Race_id,
                                 Rout_id = r.Rout_id,
                                 Driver_id = r.Driver_id,
                                 Stop_id = st.Stop_id
                                 

                             };

            var resultGrid2 = from r in db.Races.Local.ToList()
                              join b in db.Buses on r.Bus_id equals b.Bus_id
                              join d in db.Drivers on r.Driver_id equals d.Driver_id
                              join ro in db.Routes on r.Rout_id equals ro.Route_id
                              join l in db.Layouts on b.Layout_id equals l.Layout_id
                              where r.Data >= DateTime.Now

                             select new
                             {
                                 Data = r.Data.ToShortDateString(),
                                 Driver = d.Fio,
                                 Bus = b.Number,
                                 Travel_time = ro.Travel_time,                               
                                 Sets = l.Number_of_sits,
                                 Route = ro.Name_of_route,

                                 Bus_id = r.Bus_id,
                                 Race_id = r.Race_id,
                                 Rout_id = r.Rout_id,
                                 Driver_id = r.Driver_id


                             };
            var column = RaceGrid.Columns[0];

            RaceGrid.Items.SortDescriptions.Clear();

            RaceGrid.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription(column.SortMemberPath, System.ComponentModel.ListSortDirection.Descending));

            // Apply sort
            foreach (var col in RaceGrid.Columns)
            {
                col.SortDirection = null;
            }
            column.SortDirection = System.ComponentModel.ListSortDirection.Descending;
            RaceGrid.Items.Refresh();
            var column1 = RaceGrid2.Columns[0];

            RaceGrid2.Items.SortDescriptions.Clear();

            RaceGrid2.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription(column1.SortMemberPath, System.ComponentModel.ListSortDirection.Descending));

            // Apply sort
            foreach (var col in RaceGrid2.Columns)
            {
                col.SortDirection = null;
            }
            column1.SortDirection = System.ComponentModel.ListSortDirection.Descending;
            RaceGrid2.Items.Refresh();
            //var list = from  m in resultGrid.ToList()
            //           where m.Price.Max()
            //this.DataContext = listResult;
            RaceGrid.ItemsSource = resultGrid.ToList();
            RaceGrid2.ItemsSource = resultGrid2.ToList();
            db.Buses.Load();
            comboboxBus.ItemsSource = db.Buses.Local;
            db.Drivers.Load();
            Count_of_race.Text = resultGrid.ToList().Count.ToString();
            comboboxDriver.ItemsSource = db.Drivers.Local;
            db.Routes.Load();
            comboBoxRoute.ItemsSource = db.Routes.Local;
        }

        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {

            ItemSum.Visibility = Visibility.Visible;
            ItemKeyWord.Visibility = Visibility.Visible;
            ItemValue.Visibility = Visibility.Visible;
            ItemDate.Visibility = Visibility.Visible;
            ItemType.Visibility = Visibility.Visible;
            ItemUser.Visibility = Visibility.Visible;
            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
            AcceptFilter.Visibility = Visibility.Visible;
            DropFilter.Visibility = Visibility.Visible;

            //DropFilterKeyWord.Visibility = DropFilterValue.Visibility = DropFilterDate.Visibility = DropFilterSubType.Visibility = DropFilterUser.Visibility = Visibility.Visible;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            
           
            ItemKeyWord.Visibility = Visibility.Collapsed;
            ItemValue.Visibility = Visibility.Collapsed;
            ItemDate.Visibility = Visibility.Collapsed;
            ItemType.Visibility = Visibility.Collapsed;
            ItemUser.Visibility = Visibility.Collapsed;
            ItemSum.Visibility = Visibility.Collapsed;
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            ButtonOpenMenu.Visibility = Visibility.Visible;
            AcceptFilter.Visibility = Visibility.Collapsed;
            DropFilter.Visibility = Visibility.Collapsed;

            //DropFilterKeyWord.Visibility = DropFilterValue.Visibility = DropFilterDate.Visibility = DropFilterSubType.Visibility = DropFilterUser.Visibility = Visibility.Collapsed;
        }

        private void RaceGrid_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // если ни одного объекта не выделено, выходим
            if (RaceGrid.SelectedItem == null) return;
            // получаем выделенный объект

            dynamic tempR = RaceGrid.SelectedItem;
            Race race = new Race();

            race.Race_id = tempR.Race_id;
            race.Rout_id = tempR.Rout_id;
            race.Bus_id = tempR.Bus_id;
            race.Data = DateTime.Parse(tempR.Data);
            race.Driver_id = tempR.Driver_id;
            AboughtWindow w = new AboughtWindow(race);
            w.ShowDialog();

            //AddRacePage raceWindow = new AddRacePage(race);

        //    if (raceWindow.ShowDialog() == true)
        //    {
        //        // получаем измененный объект
        //        consumption = db.Consumptions.Find(consumptionWindow.Consumption.consumptionId);
        //        if (consumption != null)
        //        {
        //            consumption.consumptionId = consumptionWindow.Consumption.consumptionId;
        //            consumption.Description = consumptionWindow.Consumption.Description;
        //            consumption.SubTypeId = consumptionWindow.Consumption.SubTypeId;
        //            consumption.Value = consumptionWindow.Consumption.Value;
        //            consumption.Date = consumptionWindow.Consumption.Date;
        //            consumption.UserId = consumptionWindow.Consumption.UserId;

        //            db.Entry(consumption).State = EntityState.Modified;
        //            db.SaveChanges();
        //        }
        //        CallPopup("Трата изменена");
        //    }
        //    sum = false;
        //    if (is_FilterActivated == false)
        //        RefreshDataGrid();
        //    else AcceptFilter_Click(sender, e);
        //}
    }

        private void DeleteRace_Click(object sender, RoutedEventArgs e)
        {
            // если ни одного объекта не выделено, выходим
            if (RaceGrid.SelectedItem == null)
            {
                CallPopup("Не выбран рейс");
                return;
            }
            // получаем выделенный объект
            dynamic tempRace = RaceGrid.SelectedItem;
            int Race_id = tempRace.Race_id;
            Race r = db.Races
                .Where(c => c.Race_id == Race_id)
                .FirstOrDefault();

            var ticketsList = db.Tickets.Where(p => p.Race_id == r.Race_id);
            if (ticketsList.ToList().Count != 0)
            {
                foreach (Ticket t in ticketsList.ToList())
                    db.Tickets.Remove(t);
                CallPopup("Рейс и билеты удалены!");
            }
            else CallPopup("Рейс удален");
            db.Races.Remove(r);
            db.SaveChanges();

           
            FillDataGrid();
        }

        public void CallPopup(string text)
        {
            popupMessage.Text = text;
            Storyboard s = (Storyboard)this.TryFindResource("ShowPopup");
            BeginStoryboard(s);
        }

        private void DropFilterKeyWord_Click(object sender, RoutedEventArgs e)
        {
            comboBoxRoute.SelectedIndex=-1;
        }

        private void textblockFromValue_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void textblockToValue_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void DropFilterValue_Click(object sender, RoutedEventArgs e)
        {
            comboBoxStop.SelectedIndex = -1;
        }

        private void SetTheSameDate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DropFilterDate_Click(object sender, RoutedEventArgs e)
        {
            textblockFromPeriod.Text = textblockToPeriod.Text = "";
        }

        private void textblockFromPeriod_CalendarOpened(object sender, RoutedEventArgs e)
        {
            if (textblockFromPeriod.Text == "" && textblockToPeriod.Text == "")
            {
                textblockFromPeriod.Text = DateTime.Today.ToShortDateString();
                textblockToPeriod.Text = DateTime.Today.ToShortDateString();
            }
            textblockToPeriod.Foreground = Brushes.Black;
            textblockFromPeriod.Foreground = Brushes.Black;
        }

        private void DropFilterSubtype_Click(object sender, RoutedEventArgs e)
        {
            comboboxBus.SelectedIndex = -1;
        }

        private void DropFilterUser_Click(object sender, RoutedEventArgs e)
        {
            comboboxDriver.SelectedIndex = -1;
        }

        
        private void comboboxDriver_DropDownOpened(object sender, EventArgs e)
        {
            comboboxDriver.Foreground = Brushes.Blue;
        }

        private void comboboxDriver_DropDownClosed(object sender, EventArgs e)
        {
            comboboxDriver.Foreground = Brushes.White;           
        }

        private void AboutRace_Click(object sender, RoutedEventArgs e)
        {
            if (RaceGrid.SelectedIndex != -1)
            {
                dynamic race = RaceGrid.SelectedItem;
                WindowAboutRace rw = new WindowAboutRace(race.Race_id);
                rw.ShowDialog();
            }
            else CallPopup("Не выбран рейс");
        }

        private void BuyTicket_Click(object sender, RoutedEventArgs e)
        {
            if (RaceGrid.SelectedIndex != -1)
            {
                //dynamic race = RaceGrid.SelectedItem;
                // WindowAboutRace rw = new WindowAboutRace(race.Race_id);
                dynamic race = RaceGrid.SelectedItem;
                Bus b = db.Buses.Find(race.Bus_id);
                Layout l = db.Layouts.Find(b.Layout_id);
                int i = race.Race_id;
                var ticketsList = db.Tickets.Where(p => p.Race_id == i);
                if (int.Parse(l.Number_of_sits) == ticketsList.ToList().Count)
                {
                    MessageBoxResult result = MessageBox.Show("Все билеты на рейс распроданы!");
                }
                else
                {
                    WindowTicket tw = new WindowTicket(race.Race_id, race.Stop_id);
                    tw.ShowDialog();
                }
            }
            else CallPopup("Не выбран рейс");
        }

        private void comboboxBus_DropDownClosed(object sender, EventArgs e)
        {
            comboboxBus.Foreground = Brushes.White;
        }

        private void comboboxBus_DropDownOpened(object sender, EventArgs e)
        {
            comboboxBus.Foreground = Brushes.Blue;
        }

        private void textblockFromPeriod_CalendarClosed(object sender, RoutedEventArgs e)
        {
            textblockToPeriod.Foreground = Brushes.White;
            textblockFromPeriod.Foreground = Brushes.White;
        }

        public void Filter()
        {

        }

               
        private void comboBoxRoute_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxRoute.SelectedIndex != -1)
            {
                Route rout = (Route)comboBoxRoute.SelectedItem;
                var stopList = db.Stops.Where(p => p.Rout_id == rout.Route_id);
                comboBoxStop.ItemsSource = stopList.ToList();
            }
            else comboBoxStop.ItemsSource = null;
        }

        private void comboBoxRoute_DropDownClosed(object sender, EventArgs e)
        {
            comboBoxRoute.Foreground = Brushes.White;
        }

        private void comboBoxRoute_DropDownOpened(object sender, EventArgs e)
        {
            comboBoxRoute.Foreground = Brushes.Blue;

        }

        private void comboBoxStop_DropDownClosed(object sender, EventArgs e)
        {
            comboBoxStop.Foreground = Brushes.White;
        }

        private void comboBoxStop_DropDownOpened(object sender, EventArgs e)
        {
            comboBoxStop.Foreground = Brushes.Blue;
        }

        private void AcceptFilter_Click(object sender, RoutedEventArgs e)
        {


            using (DBContext db1 = new DBContext())
            {

                db1.Buses.Load();
                db1.Races.Load();
                db1.Drivers.Load();
                db1.Layouts.Load();
                db1.Stops.Load();
                db1.Routes.Load();


                //dynamic tempSubtype = comboboxSubType.Items[comboboxSubType.SelectedIndex];
                Route rout = (Route)comboBoxRoute.SelectedItem;
               // int tempRoutId = rout.Route_id;

                Stop stop = (Stop)comboBoxStop.SelectedItem;

               // int tempStopId = stop.Stop_id;

                Bus bus = (Bus)comboboxBus.SelectedItem;
                //int tempBusId = bus.Bus_id;

                Driver driver = (Driver)comboboxDriver.SelectedItem;
                //int tempDriverId = driver.Driver_id;

                var resultGrid = from r in db.Races.Local.ToList()
                                 join b in db.Buses on r.Bus_id equals b.Bus_id
                                 join d in db.Drivers on r.Driver_id equals d.Driver_id
                                 join ro in db.Routes on r.Rout_id equals ro.Route_id
                                 join st in db.Stops on ro.Route_id equals st.Rout_id
                                 join l in db.Layouts on b.Layout_id equals l.Layout_id

                                 where (comboBoxRoute.SelectedItem == null || ro.Route_id == rout.Route_id) &&
                                       (
                                           comboBoxStop.SelectedItem == null || st.Stop_id == stop.Stop_id
                                       ) &&
                                       (
                                           (textblockFromPeriod.Text == "" && textblockToPeriod.Text == "" ||
                                            textblockFromPeriod.Text == "1/1/0001" && textblockToPeriod.Text == "1/1/0001") ||
                                           (
                                           DateTime.Parse(textblockToPeriod.Text) >= DateTime.Parse(textblockFromPeriod.Text)
                                           &&
                                           r.Data >= DateTime.Parse(textblockFromPeriod.Text) && r.Data <= DateTime.Parse(textblockToPeriod.Text)
                                           )
                                       ) &&
                                       (comboboxDriver.SelectedItem == null || d.Driver_id == driver.Driver_id) &&
                                       (comboboxBus.SelectedItem == null || b.Bus_id == bus.Bus_id)

                                 select new
                                 {
                                     Data = r.Data.ToShortDateString(),
                                     Driver = d.Fio,
                                     Bus = b.Number,
                                     Travel_time = ro.Travel_time,
                                     Price = st.Price.ToString() + " руб.",
                                     Sets = l.Number_of_sits,
                                     Route = st.Name,

                                     Bus_id = r.Bus_id,
                                     Race_id = r.Race_id,
                                     Rout_id = r.Rout_id,
                                     Driver_id = r.Driver_id,
                                     Stop_id = st.Stop_id
                                 };

                var resultGrid2 = from r in db.Races.Local.ToList()
                                  join b in db.Buses on r.Bus_id equals b.Bus_id
                                  join d in db.Drivers on r.Driver_id equals d.Driver_id
                                  join ro in db.Routes on r.Rout_id equals ro.Route_id
                                  join l in db.Layouts on b.Layout_id equals l.Layout_id

                                  where (comboBoxRoute.SelectedItem == null || ro.Route_id == rout.Route_id)                                                                            
                                        &&
                                       (
                                           (textblockFromPeriod.Text == "" && textblockToPeriod.Text == "" ||
                                            textblockFromPeriod.Text == "1/1/0001" && textblockToPeriod.Text == "1/1/0001") ||
                                           (
                                           DateTime.Parse(textblockToPeriod.Text) >= DateTime.Parse(textblockFromPeriod.Text)
                                           &&
                                           r.Data >= DateTime.Parse(textblockFromPeriod.Text) && r.Data <= DateTime.Parse(textblockToPeriod.Text)
                                           )
                                       ) &&
                                       (comboboxDriver.SelectedItem == null || d.Driver_id == driver.Driver_id) &&
                                       (comboboxBus.SelectedItem == null || b.Bus_id == bus.Bus_id)

                                 select new
                                 {
                                     Data = r.Data.ToShortDateString(),
                                     Driver = d.Fio,
                                     Bus = b.Number,
                                     Travel_time = ro.Travel_time,
                                     Sets = l.Number_of_sits,
                                     Route = ro.Name_of_route,

                                     Bus_id = r.Bus_id,
                                     Race_id = r.Race_id,
                                     Rout_id = r.Rout_id,
                                     Driver_id = r.Driver_id
                                 };
                var listResult = resultGrid.ToList();
                RaceGrid2.ItemsSource = resultGrid2.ToList();
                Count_of_race.Text = resultGrid.ToList().Count.ToString();
                this.DataContext = listResult;
                RaceGrid.ItemsSource = listResult;
            }
        }

        private void DropFilter_Click(object sender, RoutedEventArgs e)
        {
            textblockFromPeriod.Text = textblockToPeriod.Text = "";
            comboBoxRoute.SelectedIndex = -1;
            comboboxDriver.SelectedIndex = -1;
            comboBoxStop.SelectedIndex = -1;
            comboboxBus.SelectedIndex = -1;
              
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if(RaceGrid.Visibility == Visibility.Visible)
            {
                RaceGrid.Visibility = Visibility.Hidden;
                RaceGrid2.Visibility = Visibility.Visible;
            }
            else
            {
                RaceGrid.Visibility = Visibility.Visible;
                RaceGrid2.Visibility = Visibility.Hidden;

            }
        }
    }
}
