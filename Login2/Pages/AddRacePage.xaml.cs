﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using DAL;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Login2.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddRacePage.xaml
    /// </summary>
    public partial class AddRacePage : Page
    {
        public DBContext db;
        public Race r;
        public AddRacePage()
        {
            InitializeComponent();
            db = new DBContext();
            db.Drivers.Load();
            db.Buses.Load();
            db.Routes.Load();

            fill_addPage();
        }

        void fill_addPage()
        {
            comboboxDriver.ItemsSource = db.Drivers.Local;
            comboboxBus.ItemsSource = db.Buses.Local;
            comboboxRout.ItemsSource = db.Routes.Local;
            RaceData.SelectedDate = DateTime.Now;
        }

        private void textboxAddPrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (textboxAddPrice.Text == "0")
                textboxAddPrice.Text = "";
        }

        private void textboxAddTime_KeyDown(object sender, KeyEventArgs e)
        {
            if (textboxAddTime.Text == "0")
                textboxAddTime.Text = "";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //if (textboxAddPrice.Text != "0")
            //{
            //    int resultFromValue;
            //    Int32.TryParse(textboxAddPrice.Text, out resultFromValue);
            //    if (resultFromValue == 0)
            //    {
            //        CallPopup("Некорректные данные");
            //        return;
            //    }
            //}

            //if (textboxAddTime.Text != "0")
            //{
            //    int resultFromValue;
            //    Int32.TryParse(textboxAddTime.Text, out resultFromValue);
            //    if (resultFromValue == 0)
            //    {
            //        CallPopup("Некорректные данные");
            //        return;
            //    }
            //}

            if (comboboxRout.SelectedIndex == -1 || comboboxBus.SelectedIndex == -1 || comboboxDriver.SelectedIndex == -1  )
            {
                CallPopup("Некорректные данные");
                return;
            }

            r = new Race();
            r.Data = (DateTime)RaceData.SelectedDate;
            dynamic d =  comboboxDriver.SelectedItem;
            dynamic ro = comboboxRout.SelectedItem;
            dynamic b = comboboxBus.SelectedItem;

            r.Bus_id = b.Bus_id;
            r.Driver_id = d.Driver_id;
            r.Rout_id = ro.Route_id;

            db.Races.Add(r);
            db.SaveChanges();

            comboboxRout.SelectedIndex = -1;
            comboboxBus.SelectedIndex = -1;
            comboboxDriver.SelectedIndex = -1;
            RaceData.SelectedDate = DateTime.Now;


            CallPopup("Рейс Добавлен");
        }

        public void CallPopup(string text)
        {
            popupMessage.Text = text;
            Storyboard s = (Storyboard)this.TryFindResource("ShowPopup");
            BeginStoryboard(s);
        }

        private void comboboxRout_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboboxRout.SelectedItem != null)
            {
                Route r = (Route)comboboxRout.SelectedItem;
                textboxAddTime.Text = r.Travel_time;
                var sList = db.Stops.Where(p => p.Rout_id == r.Route_id);
                textboxAddPrice.Text = sList.ToList().Count.ToString();
            }
        }
    }
}
