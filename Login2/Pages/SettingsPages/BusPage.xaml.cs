﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Windows.Media.Animation;
using DAL;
using Login2.Windows;

namespace Login2.Pages.SettingsPages
{
    /// <summary>
    /// Логика взаимодействия для BusPage.xaml
    /// </summary>
    public partial class BusPage : Page
    {
        public DBContext db;
        public BusPage()
        {
            InitializeComponent();
            db = new DBContext();
            RefreshListBoxBuses();
        }

        public void CallPopup(string text)
        {
            popupMessage.Text = text;
            Storyboard s = (Storyboard)this.TryFindResource("ShowPopup");
            BeginStoryboard(s);
        }

        public void RefreshListBoxBuses()
        {
            db = new DBContext();
            db.Buses.Load();
            db.Layouts.Load();

            var queryD = from d in db.Buses.Local
                         join lay in db.Layouts.Local
                         on d.Layout_id equals lay.Layout_id
                         select new
                         {
                             Bus_id = d.Bus_id,
                             Number = d.Number,
                             Mark = lay.Mark,
                             Number_of_sits = lay.Number_of_sits,
                             all = "Номер вытобуса: " + d.Number + "; Марка: " + lay.Mark + "; Количество мест: " + lay.Number_of_sits

                         };
            listBoxDrivers.ItemsSource = queryD.ToList();

            comboboxLayout.ItemsSource = db.Layouts.Local;
            comboboxLayout.SelectedIndex = 0;
        }

        private void AddBus(object sender, RoutedEventArgs e)
        {
            if (numberTextBox.Text != "" && numberTextBox.Text != " ")
            {
                Bus newB = new Bus();
                newB.Number = numberTextBox.Text;
                dynamic buf = comboboxLayout.SelectedItem;
                newB.Layout_id = buf.Layout_id;
                db.Buses.Add(newB);
                db.SaveChanges();

                CallPopup($"{newB.Number}\nдобавлен");

                numberTextBox.Text = ""; 
                comboboxLayout.SelectedIndex = 0;


                RefreshListBoxBuses();
            }
            else
            {
                CallPopup("Заполнены не все поля");
            }
        }

        private void DeleteBus(object sender, RoutedEventArgs e)
        {
            // если ни одного объекта не выделено, выходим
            if (listBoxDrivers.SelectedItem == null) return;
            // получаем выделенный объект

             dynamic tempR = listBoxDrivers.SelectedItem;
            Bus dr = db.Buses.Find(tempR.Bus_id);
            var rList = db.Races.Where(p => p.Bus_id == dr.Bus_id);
            if (rList.ToList().Count != 0)
            {
                foreach (Race r in rList.ToList())
                {
                    var ticketsList = db.Tickets.Where(p => p.Race_id == r.Race_id);
                    if (ticketsList.ToList().Count != 0)
                    {
                        foreach (Ticket t in ticketsList.ToList())
                            db.Tickets.Remove(t);
                    }
                    db.Races.Remove(r);
                }
                db.Buses.Remove(dr);
                CallPopup("Автобус и рейсы удалены!");
                db.SaveChanges();
                RefreshListBoxBuses();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AddLayoutWindow w = new AddLayoutWindow();
            w.ShowDialog();
            this.RefreshListBoxBuses();
        }
    }
}
