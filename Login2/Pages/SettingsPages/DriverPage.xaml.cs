﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Windows.Media.Animation;
using DAL;
using Login2.Windows;

namespace Login2.Pages.SettingsPages
{
    /// <summary>
    /// Логика взаимодействия для DriverPage.xaml
    /// </summary>
    public partial class DriverPage : Page
    {
        public DBContext db;
        public DriverPage()
        {
            InitializeComponent();
            db = new DBContext();
            RefreshListBoxUser();
        }

        public void CallPopup(string text)
        {
            popupMessage.Text = text;
            Storyboard s = (Storyboard)this.TryFindResource("ShowPopup");
            BeginStoryboard(s);
        }

        public void RefreshListBoxUser()
        {
            db = new DBContext();
            db.Drivers.Load();

            //var queryD = from d in db.Drivers.Local
            //                select new
            //                {
            //                    Fio = d.Fio,
            //                    Age = d.Age,
            //                    Driver_id = d.Driver_id,
            //                    Number_of_races = d.Number_of_races,
            //                    //all = d.Fio + " лет: " + d.Age + " количество рейсов: " + d.Number_of_races 

            //                };
            listBoxDrivers.ItemsSource = db.Drivers.Local;
        }

        private void AddUser(object sender, RoutedEventArgs e)
        {
            if (fioTextBox.Text != "" && ageTextBox.Text != ""  )
            {
                Driver newD = new Driver();
                newD.Fio = fioTextBox.Text;
                newD.Age = int.Parse(ageTextBox.Text);
                newD.Number_of_races = int.Parse(numbofRacesTextBox.Text);
                db.Drivers.Add(newD);
                db.SaveChanges();

                CallPopup($"{newD.Fio}\nдобавлен");

                fioTextBox.Text = "";  ageTextBox.Text = "";
                numbofRacesTextBox.Text = "0";

                RefreshListBoxUser();
            }
            else
            {
                CallPopup("Заполнены не все поля");
            }
        }

        private void DeleteUser(object sender, RoutedEventArgs e)
        {
            // если ни одного объекта не выделено, выходим
            if (listBoxDrivers.SelectedItem == null) return;
            // получаем выделенный объект

            // dynamic tempR = listBoxDrivers.SelectedItem;
            Driver dr = (Driver)listBoxDrivers.SelectedItem;
            var rList = db.Races.Where(p => p.Driver_id == dr.Driver_id);
            if (rList.ToList().Count != 0)
            {
                foreach (Race r in rList.ToList())
                {
                    var ticketsList = db.Tickets.Where(p => p.Race_id == r.Race_id);
                    if (ticketsList.ToList().Count != 0)
                    {
                        foreach (Ticket t in ticketsList.ToList())
                            db.Tickets.Remove(t);                      
                    }
                    db.Races.Remove(r);
                }
                db.Drivers.Remove(dr);
                CallPopup("Водитель и рейсы удалены!");
                db.SaveChanges();
            }
        }

        private void listBoxDrivers_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void listBoxDrivers_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // если ни одного объекта не выделено, выходим
            if (listBoxDrivers.SelectedItem == null) return;
            // получаем выделенный объект

           // dynamic tempR = listBoxDrivers.SelectedItem;
            Driver race = (Driver)listBoxDrivers.SelectedItem;
            DriverEd rw = new DriverEd(race);
            rw.ShowDialog();
            RefreshListBoxUser();
            
          //  MessageBox.Show("sss");

        }
    }
}
