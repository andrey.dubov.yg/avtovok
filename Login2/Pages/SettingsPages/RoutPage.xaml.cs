﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Windows.Media.Animation;
using DAL;
using Login2.Windows;

namespace Login2.Pages.SettingsPages
{
    /// <summary>
    /// Логика взаимодействия для RoutPage.xaml
    /// </summary>
    public partial class RoutPage : Page
    {
        public DBContext db;
        public RoutPage()
        {
            InitializeComponent();
            db = new DBContext();
            RefreshListBoxRouts();
        }

        public void RefreshListBoxRouts()
        {
            db.Routes.Load();
            listBoxRouts.ItemsSource = db.Routes.Local;
           
        }


        private void AddUser(object sender, RoutedEventArgs e)
        {
            //Route r = new Route();
            //r.Travel_time = "a";
            //r.Name_of_route = "ф";
            //db.Routes.Add(r);
            //db.SaveChanges();
            //r = db.Routes.Where(p => p.Name_of_route == r.Name_of_route).FirstOrDefault();
            WindowRoutAdd nw = new WindowRoutAdd();
            nw.ShowDialog();
            RefreshListBoxRouts();
        }

        private void DeleteUser(object sender, RoutedEventArgs e)
        {
            // если ни одного объекта не выделено, выходим
            if (listBoxRouts.SelectedItem == null) return;
            // получаем выделенный объект

            dynamic tempR = listBoxRouts.SelectedItem;
            Route dr = db.Routes.Find(tempR.Route_id);
            var rList = db.Races.Where(p => p.Rout_id == dr.Route_id);
            var sList = db.Stops.Where(p => p.Rout_id == dr.Route_id);
            if (rList.ToList().Count != 0)
            {
                foreach (Race r in rList.ToList())
                {
                    var ticketsList = db.Tickets.Where(p => p.Race_id == r.Race_id);
                    if (ticketsList.ToList().Count != 0)
                    {
                        foreach (Ticket t in ticketsList.ToList())
                            db.Tickets.Remove(t);
                    }
                    db.Races.Remove(r);
                }
                if (sList.ToList().Count != 0)
                {
                    foreach (Stop s in sList.ToList())
                    {
                        db.Stops.Remove(s);
                    }
                }

                db.Routes.Remove(dr);

                db.SaveChanges();
                RefreshListBoxRouts();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Вы точно хотите удалить запись из базы?", "Внимание!", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                //удалие записи из базы
                var x = listBoxRouts.SelectedIndex;
            }

        }

        private void RaceGrid_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Route r = (Route)listBoxRouts.SelectedItem;
            db.Routes.Remove(r);
        }
    }
}
