﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class BusModel : BaseModel
    {
        int Bus_id
        {
            get
            { return Bus_id; }
            set
            {
                Bus_id = value;
                OnPropertyChanged();
            }
        }

        string Number
        {
            get
            { return Number; }
            set
            {
                Number = value;
                OnPropertyChanged("Номер");
            }
        }

        int Layout_id
        {
            get
            { return Layout_id; }
            set
            {
                Layout_id = value;
                OnPropertyChanged("Id_маршрута");
            }
        }

    }
}
